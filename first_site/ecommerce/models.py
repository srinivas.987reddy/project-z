from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
# Create your models here.
class Customer(models.Model):
    name = models.CharField(max_length=200)
    user = models.CharField(max_length=100)
    
    def str(self):
        return self.name
 
class Product(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(null=True,blank=True)
    description = models.CharField(max_length=400)
    def str(self):
        return self.name
    
class Order(models.Model):
    product = models.ForeignKey( Product, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    date_ordered = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20,default= 'PENDING')
    def str(self):
        return self.name
class Merchant(models.Model):
    product = models.ForeignKey( Product, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=20,default= 'Shipped')
    def str(self):
        return self.name