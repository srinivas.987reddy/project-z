from django.shortcuts import render, redirect
from .models import Employee
# Create your views here.

#R for READ
def employee_list(request):
    employees = Employee.objects.all()
    return render(request, 'office/employee_list.html', 
                  {'employees': employees})


#C for CREATE
def add_employee(request):
    if request.method == 'GET':
        return render(request, 'office/add_employee.html')
    else:
        name_ = request.POST['name']
        emp_num_ = request.POST['emp_num']
        salary_ = request.POST['salary']

        e = Employee(
            name = name_,
            emp_num = emp_num_,
            salary = salary_
        )
        e.save()
        return redirect('employee_list')

#R for READ    
def employee_detail(request, id):
    e = Employee.objects.get(id=id)
    return render(request, 'office/employee_detail.html',
                  {'employee': e})

#D for DELETE 
def delete_employee(request, id):
    e = Employee.objects.get(id=id)
    e.delete()
    return redirect('employee_list')

#U for UPDATE
def update_employee(request, id):
    if request.method == 'GET':
        e = Employee.objects.get(id=id)
        editing = True
        context = {'employee': e, 'editing': editing}
        return render(request, 'office/employee_detail.html', context)

    if request.method == 'POST':
        e = Employee.objects.get(id=id)
        e.name = request.POST['name']
        e.salary = request.POST['salary']
        e.emp_num = request.POST['emp_num']
        e.save()
        return redirect('employee_detail', id)