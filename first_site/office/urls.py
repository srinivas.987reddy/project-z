from django.urls import path
from . import views
urlpatterns = [
    path('all', views.employee_list, name='employee_list'),
    path('add', views.add_employee, name='add_employee'),
    path('detail/<int:id>', views.employee_detail, name='employee_detail'),
    path('delete/<int:id>', views.delete_employee, name='delete_employee'),
    path('edit/<int:id>', views.update_employee, name='update_employee'),
]