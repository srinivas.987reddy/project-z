from django.db import models

# Create your models here.

class Employee(models.Model):
    name = models.CharField(max_length = 100)
    emp_num = models.CharField(max_length = 10)
    salary = models.CharField(max_length = 30)

    def _str_(self):
        return f'{self.name} - {self.emp_num} - {self.salary}'

    
